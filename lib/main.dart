import 'package:flutter/material.dart';
// import 'package:image_upload/demo_upload_image.dart';
import 'package:image_upload/multiple_image_upload.dart';
// import 'package:image_upload/upload_image_demo.dart';

import 'image_picker_demo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultipleImageUpload(),
    );
  }
}
