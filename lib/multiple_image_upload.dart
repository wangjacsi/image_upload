import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:http/http.dart' as http;

class MultipleImageUpload extends StatefulWidget {
  @override
  _MultipleImageUploadState createState() => _MultipleImageUploadState();
}

class _MultipleImageUploadState extends State<MultipleImageUpload> {
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  // List<File> files = [];
  List files = [];

  getImageFileFromAsset(String path) {
    final file = File(path);
    return file;
  }

  _submit() async {
    files = [];
    for (int i = 0; i < images.length; i++) {
      var path2 =
          await FlutterAbsolutePath.getAbsolutePath(images[i].identifier);
      var file = await getImageFileFromAsset(path2);
      var base64Image = base64Encode(file.readAsBytesSync());
      files.add(base64Image);
    }

    var data = {
      "photos": files,
      "comment": 'asdasd',
      "ratings": '45',
      'writer': 'asdasds'
    };
    try {
      final headers = {
        'content-type': 'application/json',
        'authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1OTkwZGUyNWI3NzVjNmNlNDExM2I2NTEwYWQ1ZjJlZGQ3ZjhiYWE1YmI1N2Q5OTdmMGQyZDcwNmQ0MzA1YmZkMGJlNTNmODVmZjEzMWE3In0.eyJhdWQiOiIzIiwianRpIjoiYTU5OTBkZTI1Yjc3NWM2Y2U0MTEzYjY1MTBhZDVmMmVkZDdmOGJhYTViYjU3ZDk5N2YwZDJkNzA2ZDQzMDViZmQwYmU1M2Y4NWZmMTMxYTciLCJpYXQiOjE2MDc5MTQzNjIsIm5iZiI6MTYwNzkxNDM2MiwiZXhwIjoxNjM5NDUwMzYyLCJzdWIiOiIxNTk4Iiwic2NvcGVzIjpbXX0.Lb7CGbrIGESnBOtY7emYhM07rErMSNhEx2BzgmQRbIdxL6P3ZjqkOW19NZY4yyhB20_zir0qovfTSTW2RupXzwZ1j-6scNBNV3-HF-Fa3kp6c9sYF9QWwrFmVGH8vPCncwai0FCkfONetkn1RWFCaMw_AwYDSELfsxtvCa9SfzL3DUXbQ06jed4Ys7a2evfhlWoJCrtGHi-J91zKuJwEk_pBCLD9aOnQCHau7vObZn9_T1qcQNBegVUlXJZrMJ62k_ocEhqvI1tCQ8L1niqSvGMZjXb7f3lCq4ilCJVWFC5gJDGj-y4QxrICXtyCZiYsIiZ2e7kaVQuQ27-9c9aspr-WCkYgT4aXS-sXQyTnOHrH7S6XBvZrVZezxgmci1G1U4bu1Fo6Z2nHRVcsQ4MpLjuKSerB1vqha8A8y0bH3nn12Fp--3F-eDESRtTV4vLwHIeKY_aV5yU-uL1CMGL83K-Hg6uKG4_eMU9zxwUKPNDCx0KuMxb8h7z0x1WeMWuNhdAPoSYMIqUIAbAx7AOFEPOKg8yIHTIx4IiuVsNNhmhpviRmAicFkgUYFz36h38QPsL6jloZdkpggTnl3iXMFSvDGjH7C9i_eiHgMyywrzEksoLnoSl6zGss96kplYDVRk5WZYZK4lOnWypkVZgBlNH5C2TWGxTvgDG5m1wzcEM',
      };

      final response = await http.post(
          'http://vbapiserver.test/api/item/4200/review',
          body: json.encode(data),
          headers: headers);
      print(response.statusCode);
      print(response.body);
      var body = jsonDecode(response.body);
      print(body);
    } catch (e) {
      print(e.toString());
      return e.message;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: <Widget>[
            Center(child: Text('Error: $_error')),
            RaisedButton(
              child: Text("Pick images"),
              onPressed: loadAssets,
            ),
            Expanded(
              child: buildGridView(),
            ),
            RaisedButton(
              child: Text("Upload images"),
              onPressed: _submit,
            ),
          ],
        ),
      ),
    );
  }
}
