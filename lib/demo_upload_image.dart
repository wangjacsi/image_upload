import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path/path.dart';
import 'package:async/async.dart';

class DemoUploadImage extends StatefulWidget {
  @override
  _DemoUploadImageState createState() => _DemoUploadImageState();
}

class _DemoUploadImageState extends State<DemoUploadImage> {
  File _image;
  final picker = ImagePicker();
  TextEditingController nameController = TextEditingController();

  Future choiceImage() async {
    var pickedImage = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedImage.path);
    });
  }

  final String _url =
      "https://billyapi.com/api/"; //"http://vbapiserver.test/api/"; //"https://billyapi.com/api/";
  Response response;
  Map<String, dynamic> body = {};
  Map<String, String> header = {};
  var responseData;
  // Future<http.StreamedResponse>
  Future uploadImage() async {
    final uri = Uri.parse('http://vbapiserver.test/api/item/4200/review');
    var request = http.MultipartRequest('POST', uri);
    // request.headers['authorization'] =
    //     'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1OTkwZGUyNWI3NzVjNmNlNDExM2I2NTEwYWQ1ZjJlZGQ3ZjhiYWE1YmI1N2Q5OTdmMGQyZDcwNmQ0MzA1YmZkMGJlNTNmODVmZjEzMWE3In0.eyJhdWQiOiIzIiwianRpIjoiYTU5OTBkZTI1Yjc3NWM2Y2U0MTEzYjY1MTBhZDVmMmVkZDdmOGJhYTViYjU3ZDk5N2YwZDJkNzA2ZDQzMDViZmQwYmU1M2Y4NWZmMTMxYTciLCJpYXQiOjE2MDc5MTQzNjIsIm5iZiI6MTYwNzkxNDM2MiwiZXhwIjoxNjM5NDUwMzYyLCJzdWIiOiIxNTk4Iiwic2NvcGVzIjpbXX0.Lb7CGbrIGESnBOtY7emYhM07rErMSNhEx2BzgmQRbIdxL6P3ZjqkOW19NZY4yyhB20_zir0qovfTSTW2RupXzwZ1j-6scNBNV3-HF-Fa3kp6c9sYF9QWwrFmVGH8vPCncwai0FCkfONetkn1RWFCaMw_AwYDSELfsxtvCa9SfzL3DUXbQ06jed4Ys7a2evfhlWoJCrtGHi-J91zKuJwEk_pBCLD9aOnQCHau7vObZn9_T1qcQNBegVUlXJZrMJ62k_ocEhqvI1tCQ8L1niqSvGMZjXb7f3lCq4ilCJVWFC5gJDGj-y4QxrICXtyCZiYsIiZ2e7kaVQuQ27-9c9aspr-WCkYgT4aXS-sXQyTnOHrH7S6XBvZrVZezxgmci1G1U4bu1Fo6Z2nHRVcsQ4MpLjuKSerB1vqha8A8y0bH3nn12Fp--3F-eDESRtTV4vLwHIeKY_aV5yU-uL1CMGL83K-Hg6uKG4_eMU9zxwUKPNDCx0KuMxb8h7z0x1WeMWuNhdAPoSYMIqUIAbAx7AOFEPOKg8yIHTIx4IiuVsNNhmhpviRmAicFkgUYFz36h38QPsL6jloZdkpggTnl3iXMFSvDGjH7C9i_eiHgMyywrzEksoLnoSl6zGss96kplYDVRk5WZYZK4lOnWypkVZgBlNH5C2TWGxTvgDG5m1wzcEM';
    request.fields['comment'] = nameController.text;
    request.fields['ratings'] = '45';
    request.fields['writer'] = 'ㅁㄴㅇㅁㄴㅇㅁㄴ ';
    String fileName = _image.path.split('/').last;

    List<MultipartFile> newList = new List<MultipartFile>();
    for (int i = 0; i < 1; i++) {
      // File imageFile = _image; //File(_image.toString());
      // var stream = new http.ByteStream(imageFile.openRead());
      // // stream.cast();
      // // var stream =
      // //     new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      // var length = await imageFile.length();
      // var multipartFile = new http.MultipartFile("photos", stream, length,
      //     filename: basename(imageFile.path));
      // newList.add(multipartFile);

      var pic = await http.MultipartFile.fromPath(
        'photos',
        _image.path,
        filename: fileName,
      );
      newList.add(pic);
    }

    request.files.addAll(newList);
    // var pic = await http.MultipartFile.fromPath(
    //   'photos',
    //   _image.path,
    //   filename: fileName,
    // );
    //
    // request.files.add(pic);

    request.headers.addAll({
      "Content-type": "multipart/form-data",
      "Authorization":
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1OTkwZGUyNWI3NzVjNmNlNDExM2I2NTEwYWQ1ZjJlZGQ3ZjhiYWE1YmI1N2Q5OTdmMGQyZDcwNmQ0MzA1YmZkMGJlNTNmODVmZjEzMWE3In0.eyJhdWQiOiIzIiwianRpIjoiYTU5OTBkZTI1Yjc3NWM2Y2U0MTEzYjY1MTBhZDVmMmVkZDdmOGJhYTViYjU3ZDk5N2YwZDJkNzA2ZDQzMDViZmQwYmU1M2Y4NWZmMTMxYTciLCJpYXQiOjE2MDc5MTQzNjIsIm5iZiI6MTYwNzkxNDM2MiwiZXhwIjoxNjM5NDUwMzYyLCJzdWIiOiIxNTk4Iiwic2NvcGVzIjpbXX0.Lb7CGbrIGESnBOtY7emYhM07rErMSNhEx2BzgmQRbIdxL6P3ZjqkOW19NZY4yyhB20_zir0qovfTSTW2RupXzwZ1j-6scNBNV3-HF-Fa3kp6c9sYF9QWwrFmVGH8vPCncwai0FCkfONetkn1RWFCaMw_AwYDSELfsxtvCa9SfzL3DUXbQ06jed4Ys7a2evfhlWoJCrtGHi-J91zKuJwEk_pBCLD9aOnQCHau7vObZn9_T1qcQNBegVUlXJZrMJ62k_ocEhqvI1tCQ8L1niqSvGMZjXb7f3lCq4ilCJVWFC5gJDGj-y4QxrICXtyCZiYsIiZ2e7kaVQuQ27-9c9aspr-WCkYgT4aXS-sXQyTnOHrH7S6XBvZrVZezxgmci1G1U4bu1Fo6Z2nHRVcsQ4MpLjuKSerB1vqha8A8y0bH3nn12Fp--3F-eDESRtTV4vLwHIeKY_aV5yU-uL1CMGL83K-Hg6uKG4_eMU9zxwUKPNDCx0KuMxb8h7z0x1WeMWuNhdAPoSYMIqUIAbAx7AOFEPOKg8yIHTIx4IiuVsNNhmhpviRmAicFkgUYFz36h38QPsL6jloZdkpggTnl3iXMFSvDGjH7C9i_eiHgMyywrzEksoLnoSl6zGss96kplYDVRk5WZYZK4lOnWypkVZgBlNH5C2TWGxTvgDG5m1wzcEM',
    });

    var response = await request.send();
    print(response.statusCode);
    print(response.stream.bytesToString());
    if (response.statusCode == 200) {
      print('Image Uploaded');
    } else {
      print('Image Not Uploaded');
    }

    // header = {
    //   'authorization':
    //       'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1OTkwZGUyNWI3NzVjNmNlNDExM2I2NTEwYWQ1ZjJlZGQ3ZjhiYWE1YmI1N2Q5OTdmMGQyZDcwNmQ0MzA1YmZkMGJlNTNmODVmZjEzMWE3In0.eyJhdWQiOiIzIiwianRpIjoiYTU5OTBkZTI1Yjc3NWM2Y2U0MTEzYjY1MTBhZDVmMmVkZDdmOGJhYTViYjU3ZDk5N2YwZDJkNzA2ZDQzMDViZmQwYmU1M2Y4NWZmMTMxYTciLCJpYXQiOjE2MDc5MTQzNjIsIm5iZiI6MTYwNzkxNDM2MiwiZXhwIjoxNjM5NDUwMzYyLCJzdWIiOiIxNTk4Iiwic2NvcGVzIjpbXX0.Lb7CGbrIGESnBOtY7emYhM07rErMSNhEx2BzgmQRbIdxL6P3ZjqkOW19NZY4yyhB20_zir0qovfTSTW2RupXzwZ1j-6scNBNV3-HF-Fa3kp6c9sYF9QWwrFmVGH8vPCncwai0FCkfONetkn1RWFCaMw_AwYDSELfsxtvCa9SfzL3DUXbQ06jed4Ys7a2evfhlWoJCrtGHi-J91zKuJwEk_pBCLD9aOnQCHau7vObZn9_T1qcQNBegVUlXJZrMJ62k_ocEhqvI1tCQ8L1niqSvGMZjXb7f3lCq4ilCJVWFC5gJDGj-y4QxrICXtyCZiYsIiZ2e7kaVQuQ27-9c9aspr-WCkYgT4aXS-sXQyTnOHrH7S6XBvZrVZezxgmci1G1U4bu1Fo6Z2nHRVcsQ4MpLjuKSerB1vqha8A8y0bH3nn12Fp--3F-eDESRtTV4vLwHIeKY_aV5yU-uL1CMGL83K-Hg6uKG4_eMU9zxwUKPNDCx0KuMxb8h7z0x1WeMWuNhdAPoSYMIqUIAbAx7AOFEPOKg8yIHTIx4IiuVsNNhmhpviRmAicFkgUYFz36h38QPsL6jloZdkpggTnl3iXMFSvDGjH7C9i_eiHgMyywrzEksoLnoSl6zGss96kplYDVRk5WZYZK4lOnWypkVZgBlNH5C2TWGxTvgDG5m1wzcEM',
    // };
    //
    //
    // body = {
    //   'ratings': '45',
    //   'comment': 'asdasdasd',
    //   'photos': , //'$photos',
    // };
    // await _runWithErrorHandling(() async {
    //   response = await http.post("http://vbapiserver.test/api/item/4201/review",
    //       headers: header, body: body);
    //   print("리뷰 데이터 생성 :${response.statusCode}");
    //   print(response.body);
    //   if (response.statusCode == CommunicationState.SUCCESS_OK.value) {
    //     responseData = jsonDecode(response.body);
    //   } else {
    //     responseData = {};
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Upload Image'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(labelText: 'Name'),
              ),
            ),
            IconButton(
              icon: Icon(Icons.camera),
              onPressed: () {
                choiceImage();
              },
            ),
            Container(
              child: _image == null
                  ? Text('No Image Selected')
                  : Image.file(_image),
            ),
            SizedBox(
              height: 10,
            ),
            RaisedButton(
              onPressed: () {
                uploadImage();
              },
              child: Text('Upload Image'),
            )
          ],
        ),
      ),
    );
  }
}
